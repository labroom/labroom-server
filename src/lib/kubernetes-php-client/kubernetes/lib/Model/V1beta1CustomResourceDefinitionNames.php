<?php
/**
 * V1beta1CustomResourceDefinitionNames
 *
 * PHP version 5
 *
 * @category Class
 * @package  CBSi\Kubernetes
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Kubernetes
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.13.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace CBSi\Kubernetes\Model;

use \ArrayAccess;
use \CBSi\Kubernetes\ObjectSerializer;

/**
 * V1beta1CustomResourceDefinitionNames Class Doc Comment
 *
 * @category Class
 * @description CustomResourceDefinitionNames indicates the names to serve this CustomResourceDefinition
 * @package  CBSi\Kubernetes
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class V1beta1CustomResourceDefinitionNames implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'v1beta1.CustomResourceDefinitionNames';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'categories' => 'string[]',
        'kind' => 'string',
        'list_kind' => 'string',
        'plural' => 'string',
        'short_names' => 'string[]',
        'singular' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'categories' => null,
        'kind' => null,
        'list_kind' => null,
        'plural' => null,
        'short_names' => null,
        'singular' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'categories' => 'categories',
        'kind' => 'kind',
        'list_kind' => 'listKind',
        'plural' => 'plural',
        'short_names' => 'shortNames',
        'singular' => 'singular'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'categories' => 'setCategories',
        'kind' => 'setKind',
        'list_kind' => 'setListKind',
        'plural' => 'setPlural',
        'short_names' => 'setShortNames',
        'singular' => 'setSingular'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'categories' => 'getCategories',
        'kind' => 'getKind',
        'list_kind' => 'getListKind',
        'plural' => 'getPlural',
        'short_names' => 'getShortNames',
        'singular' => 'getSingular'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['categories'] = isset($data['categories']) ? $data['categories'] : null;
        $this->container['kind'] = isset($data['kind']) ? $data['kind'] : null;
        $this->container['list_kind'] = isset($data['list_kind']) ? $data['list_kind'] : null;
        $this->container['plural'] = isset($data['plural']) ? $data['plural'] : null;
        $this->container['short_names'] = isset($data['short_names']) ? $data['short_names'] : null;
        $this->container['singular'] = isset($data['singular']) ? $data['singular'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['kind'] === null) {
            $invalidProperties[] = "'kind' can't be null";
        }
        if ($this->container['plural'] === null) {
            $invalidProperties[] = "'plural' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {

        if ($this->container['kind'] === null) {
            return false;
        }
        if ($this->container['plural'] === null) {
            return false;
        }
        return true;
    }


    /**
     * Gets categories
     *
     * @return string[]
     */
    public function getCategories()
    {
        return $this->container['categories'];
    }

    /**
     * Sets categories
     *
     * @param string[] $categories Categories is a list of grouped resources custom resources belong to (e.g. 'all')
     *
     * @return $this
     */
    public function setCategories($categories)
    {
        $this->container['categories'] = $categories;

        return $this;
    }

    /**
     * Gets kind
     *
     * @return string
     */
    public function getKind()
    {
        return $this->container['kind'];
    }

    /**
     * Sets kind
     *
     * @param string $kind Kind is the serialized kind of the resource.  It is normally CamelCase and singular.
     *
     * @return $this
     */
    public function setKind($kind)
    {
        $this->container['kind'] = $kind;

        return $this;
    }

    /**
     * Gets list_kind
     *
     * @return string
     */
    public function getListKind()
    {
        return $this->container['list_kind'];
    }

    /**
     * Sets list_kind
     *
     * @param string $list_kind ListKind is the serialized kind of the list for this resource.  Defaults to <kind>List.
     *
     * @return $this
     */
    public function setListKind($list_kind)
    {
        $this->container['list_kind'] = $list_kind;

        return $this;
    }

    /**
     * Gets plural
     *
     * @return string
     */
    public function getPlural()
    {
        return $this->container['plural'];
    }

    /**
     * Sets plural
     *
     * @param string $plural Plural is the plural name of the resource to serve.  It must match the name of the CustomResourceDefinition-registration too: plural.group and it must be all lowercase.
     *
     * @return $this
     */
    public function setPlural($plural)
    {
        $this->container['plural'] = $plural;

        return $this;
    }

    /**
     * Gets short_names
     *
     * @return string[]
     */
    public function getShortNames()
    {
        return $this->container['short_names'];
    }

    /**
     * Sets short_names
     *
     * @param string[] $short_names ShortNames are short names for the resource.  It must be all lowercase.
     *
     * @return $this
     */
    public function setShortNames($short_names)
    {
        $this->container['short_names'] = $short_names;

        return $this;
    }

    /**
     * Gets singular
     *
     * @return string
     */
    public function getSingular()
    {
        return $this->container['singular'];
    }

    /**
     * Sets singular
     *
     * @param string $singular Singular is the singular name of the resource.  It must be all lowercase  Defaults to lowercased <kind>
     *
     * @return $this
     */
    public function setSingular($singular)
    {
        $this->container['singular'] = $singular;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


