<?php
/**
 * AppsV1ApiTest
 * PHP version 5
 *
 * @category Class
 * @package  CBSi\Kubernetes
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Kubernetes
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.13.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace CBSi\Kubernetes;

use \CBSi\Kubernetes\Configuration;
use \CBSi\Kubernetes\ApiException;
use \CBSi\Kubernetes\ObjectSerializer;

/**
 * AppsV1ApiTest Class Doc Comment
 *
 * @category Class
 * @package  CBSi\Kubernetes
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class AppsV1ApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createNamespacedControllerRevision
     *
     * .
     *
     */
    public function testCreateNamespacedControllerRevision()
    {
    }

    /**
     * Test case for createNamespacedDaemonSet
     *
     * .
     *
     */
    public function testCreateNamespacedDaemonSet()
    {
    }

    /**
     * Test case for createNamespacedDeployment
     *
     * .
     *
     */
    public function testCreateNamespacedDeployment()
    {
    }

    /**
     * Test case for createNamespacedReplicaSet
     *
     * .
     *
     */
    public function testCreateNamespacedReplicaSet()
    {
    }

    /**
     * Test case for createNamespacedStatefulSet
     *
     * .
     *
     */
    public function testCreateNamespacedStatefulSet()
    {
    }

    /**
     * Test case for deleteCollectionNamespacedControllerRevision
     *
     * .
     *
     */
    public function testDeleteCollectionNamespacedControllerRevision()
    {
    }

    /**
     * Test case for deleteCollectionNamespacedDaemonSet
     *
     * .
     *
     */
    public function testDeleteCollectionNamespacedDaemonSet()
    {
    }

    /**
     * Test case for deleteCollectionNamespacedDeployment
     *
     * .
     *
     */
    public function testDeleteCollectionNamespacedDeployment()
    {
    }

    /**
     * Test case for deleteCollectionNamespacedReplicaSet
     *
     * .
     *
     */
    public function testDeleteCollectionNamespacedReplicaSet()
    {
    }

    /**
     * Test case for deleteCollectionNamespacedStatefulSet
     *
     * .
     *
     */
    public function testDeleteCollectionNamespacedStatefulSet()
    {
    }

    /**
     * Test case for deleteNamespacedControllerRevision
     *
     * .
     *
     */
    public function testDeleteNamespacedControllerRevision()
    {
    }

    /**
     * Test case for deleteNamespacedDaemonSet
     *
     * .
     *
     */
    public function testDeleteNamespacedDaemonSet()
    {
    }

    /**
     * Test case for deleteNamespacedDeployment
     *
     * .
     *
     */
    public function testDeleteNamespacedDeployment()
    {
    }

    /**
     * Test case for deleteNamespacedReplicaSet
     *
     * .
     *
     */
    public function testDeleteNamespacedReplicaSet()
    {
    }

    /**
     * Test case for deleteNamespacedStatefulSet
     *
     * .
     *
     */
    public function testDeleteNamespacedStatefulSet()
    {
    }

    /**
     * Test case for getAPIResources
     *
     * .
     *
     */
    public function testGetAPIResources()
    {
    }

    /**
     * Test case for listControllerRevisionForAllNamespaces
     *
     * .
     *
     */
    public function testListControllerRevisionForAllNamespaces()
    {
    }

    /**
     * Test case for listDaemonSetForAllNamespaces
     *
     * .
     *
     */
    public function testListDaemonSetForAllNamespaces()
    {
    }

    /**
     * Test case for listDeploymentForAllNamespaces
     *
     * .
     *
     */
    public function testListDeploymentForAllNamespaces()
    {
    }

    /**
     * Test case for listNamespacedControllerRevision
     *
     * .
     *
     */
    public function testListNamespacedControllerRevision()
    {
    }

    /**
     * Test case for listNamespacedDaemonSet
     *
     * .
     *
     */
    public function testListNamespacedDaemonSet()
    {
    }

    /**
     * Test case for listNamespacedDeployment
     *
     * .
     *
     */
    public function testListNamespacedDeployment()
    {
    }

    /**
     * Test case for listNamespacedReplicaSet
     *
     * .
     *
     */
    public function testListNamespacedReplicaSet()
    {
    }

    /**
     * Test case for listNamespacedStatefulSet
     *
     * .
     *
     */
    public function testListNamespacedStatefulSet()
    {
    }

    /**
     * Test case for listReplicaSetForAllNamespaces
     *
     * .
     *
     */
    public function testListReplicaSetForAllNamespaces()
    {
    }

    /**
     * Test case for listStatefulSetForAllNamespaces
     *
     * .
     *
     */
    public function testListStatefulSetForAllNamespaces()
    {
    }

    /**
     * Test case for patchNamespacedControllerRevision
     *
     * .
     *
     */
    public function testPatchNamespacedControllerRevision()
    {
    }

    /**
     * Test case for patchNamespacedDaemonSet
     *
     * .
     *
     */
    public function testPatchNamespacedDaemonSet()
    {
    }

    /**
     * Test case for patchNamespacedDaemonSetStatus
     *
     * .
     *
     */
    public function testPatchNamespacedDaemonSetStatus()
    {
    }

    /**
     * Test case for patchNamespacedDeployment
     *
     * .
     *
     */
    public function testPatchNamespacedDeployment()
    {
    }

    /**
     * Test case for patchNamespacedDeploymentScale
     *
     * .
     *
     */
    public function testPatchNamespacedDeploymentScale()
    {
    }

    /**
     * Test case for patchNamespacedDeploymentStatus
     *
     * .
     *
     */
    public function testPatchNamespacedDeploymentStatus()
    {
    }

    /**
     * Test case for patchNamespacedReplicaSet
     *
     * .
     *
     */
    public function testPatchNamespacedReplicaSet()
    {
    }

    /**
     * Test case for patchNamespacedReplicaSetScale
     *
     * .
     *
     */
    public function testPatchNamespacedReplicaSetScale()
    {
    }

    /**
     * Test case for patchNamespacedReplicaSetStatus
     *
     * .
     *
     */
    public function testPatchNamespacedReplicaSetStatus()
    {
    }

    /**
     * Test case for patchNamespacedStatefulSet
     *
     * .
     *
     */
    public function testPatchNamespacedStatefulSet()
    {
    }

    /**
     * Test case for patchNamespacedStatefulSetScale
     *
     * .
     *
     */
    public function testPatchNamespacedStatefulSetScale()
    {
    }

    /**
     * Test case for patchNamespacedStatefulSetStatus
     *
     * .
     *
     */
    public function testPatchNamespacedStatefulSetStatus()
    {
    }

    /**
     * Test case for readNamespacedControllerRevision
     *
     * .
     *
     */
    public function testReadNamespacedControllerRevision()
    {
    }

    /**
     * Test case for readNamespacedDaemonSet
     *
     * .
     *
     */
    public function testReadNamespacedDaemonSet()
    {
    }

    /**
     * Test case for readNamespacedDaemonSetStatus
     *
     * .
     *
     */
    public function testReadNamespacedDaemonSetStatus()
    {
    }

    /**
     * Test case for readNamespacedDeployment
     *
     * .
     *
     */
    public function testReadNamespacedDeployment()
    {
    }

    /**
     * Test case for readNamespacedDeploymentScale
     *
     * .
     *
     */
    public function testReadNamespacedDeploymentScale()
    {
    }

    /**
     * Test case for readNamespacedDeploymentStatus
     *
     * .
     *
     */
    public function testReadNamespacedDeploymentStatus()
    {
    }

    /**
     * Test case for readNamespacedReplicaSet
     *
     * .
     *
     */
    public function testReadNamespacedReplicaSet()
    {
    }

    /**
     * Test case for readNamespacedReplicaSetScale
     *
     * .
     *
     */
    public function testReadNamespacedReplicaSetScale()
    {
    }

    /**
     * Test case for readNamespacedReplicaSetStatus
     *
     * .
     *
     */
    public function testReadNamespacedReplicaSetStatus()
    {
    }

    /**
     * Test case for readNamespacedStatefulSet
     *
     * .
     *
     */
    public function testReadNamespacedStatefulSet()
    {
    }

    /**
     * Test case for readNamespacedStatefulSetScale
     *
     * .
     *
     */
    public function testReadNamespacedStatefulSetScale()
    {
    }

    /**
     * Test case for readNamespacedStatefulSetStatus
     *
     * .
     *
     */
    public function testReadNamespacedStatefulSetStatus()
    {
    }

    /**
     * Test case for replaceNamespacedControllerRevision
     *
     * .
     *
     */
    public function testReplaceNamespacedControllerRevision()
    {
    }

    /**
     * Test case for replaceNamespacedDaemonSet
     *
     * .
     *
     */
    public function testReplaceNamespacedDaemonSet()
    {
    }

    /**
     * Test case for replaceNamespacedDaemonSetStatus
     *
     * .
     *
     */
    public function testReplaceNamespacedDaemonSetStatus()
    {
    }

    /**
     * Test case for replaceNamespacedDeployment
     *
     * .
     *
     */
    public function testReplaceNamespacedDeployment()
    {
    }

    /**
     * Test case for replaceNamespacedDeploymentScale
     *
     * .
     *
     */
    public function testReplaceNamespacedDeploymentScale()
    {
    }

    /**
     * Test case for replaceNamespacedDeploymentStatus
     *
     * .
     *
     */
    public function testReplaceNamespacedDeploymentStatus()
    {
    }

    /**
     * Test case for replaceNamespacedReplicaSet
     *
     * .
     *
     */
    public function testReplaceNamespacedReplicaSet()
    {
    }

    /**
     * Test case for replaceNamespacedReplicaSetScale
     *
     * .
     *
     */
    public function testReplaceNamespacedReplicaSetScale()
    {
    }

    /**
     * Test case for replaceNamespacedReplicaSetStatus
     *
     * .
     *
     */
    public function testReplaceNamespacedReplicaSetStatus()
    {
    }

    /**
     * Test case for replaceNamespacedStatefulSet
     *
     * .
     *
     */
    public function testReplaceNamespacedStatefulSet()
    {
    }

    /**
     * Test case for replaceNamespacedStatefulSetScale
     *
     * .
     *
     */
    public function testReplaceNamespacedStatefulSetScale()
    {
    }

    /**
     * Test case for replaceNamespacedStatefulSetStatus
     *
     * .
     *
     */
    public function testReplaceNamespacedStatefulSetStatus()
    {
    }
}
