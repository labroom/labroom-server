<?php
/**
 * V1ResourceRuleTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  CBSi\Kubernetes
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Kubernetes
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.13.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace CBSi\Kubernetes;

/**
 * V1ResourceRuleTest Class Doc Comment
 *
 * @category    Class */
// * @description ResourceRule is the list of actions the subject is allowed to perform on resources. The list ordering isn&#39;t significant, may contain duplicates, and possibly be incomplete.
/**
 * @package     CBSi\Kubernetes
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class V1ResourceRuleTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "V1ResourceRule"
     */
    public function testV1ResourceRule()
    {
    }

    /**
     * Test attribute "api_groups"
     */
    public function testPropertyApiGroups()
    {
    }

    /**
     * Test attribute "resource_names"
     */
    public function testPropertyResourceNames()
    {
    }

    /**
     * Test attribute "resources"
     */
    public function testPropertyResources()
    {
    }

    /**
     * Test attribute "verbs"
     */
    public function testPropertyVerbs()
    {
    }
}
