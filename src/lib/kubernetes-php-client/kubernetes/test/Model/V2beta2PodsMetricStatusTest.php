<?php
/**
 * V2beta2PodsMetricStatusTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  CBSi\Kubernetes
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Kubernetes
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.13.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace CBSi\Kubernetes;

/**
 * V2beta2PodsMetricStatusTest Class Doc Comment
 *
 * @category    Class */
// * @description PodsMetricStatus indicates the current value of a metric describing each pod in the current scale target (for example, transactions-processed-per-second).
/**
 * @package     CBSi\Kubernetes
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class V2beta2PodsMetricStatusTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "V2beta2PodsMetricStatus"
     */
    public function testV2beta2PodsMetricStatus()
    {
    }

    /**
     * Test attribute "current"
     */
    public function testPropertyCurrent()
    {
    }

    /**
     * Test attribute "metric"
     */
    public function testPropertyMetric()
    {
    }
}
