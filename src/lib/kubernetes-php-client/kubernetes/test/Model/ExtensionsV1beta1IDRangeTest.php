<?php
/**
 * ExtensionsV1beta1IDRangeTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  CBSi\Kubernetes
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Kubernetes
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.13.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace CBSi\Kubernetes;

/**
 * ExtensionsV1beta1IDRangeTest Class Doc Comment
 *
 * @category    Class */
// * @description IDRange provides a min/max of an allowed range of IDs. Deprecated: use IDRange from policy API Group instead.
/**
 * @package     CBSi\Kubernetes
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ExtensionsV1beta1IDRangeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ExtensionsV1beta1IDRange"
     */
    public function testExtensionsV1beta1IDRange()
    {
    }

    /**
     * Test attribute "max"
     */
    public function testPropertyMax()
    {
    }

    /**
     * Test attribute "min"
     */
    public function testPropertyMin()
    {
    }
}
