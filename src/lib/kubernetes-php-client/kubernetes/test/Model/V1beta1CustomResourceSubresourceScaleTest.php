<?php
/**
 * V1beta1CustomResourceSubresourceScaleTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  CBSi\Kubernetes
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Kubernetes
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.13.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace CBSi\Kubernetes;

/**
 * V1beta1CustomResourceSubresourceScaleTest Class Doc Comment
 *
 * @category    Class */
// * @description CustomResourceSubresourceScale defines how to serve the scale subresource for CustomResources.
/**
 * @package     CBSi\Kubernetes
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class V1beta1CustomResourceSubresourceScaleTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "V1beta1CustomResourceSubresourceScale"
     */
    public function testV1beta1CustomResourceSubresourceScale()
    {
    }

    /**
     * Test attribute "label_selector_path"
     */
    public function testPropertyLabelSelectorPath()
    {
    }

    /**
     * Test attribute "spec_replicas_path"
     */
    public function testPropertySpecReplicasPath()
    {
    }

    /**
     * Test attribute "status_replicas_path"
     */
    public function testPropertyStatusReplicasPath()
    {
    }
}
